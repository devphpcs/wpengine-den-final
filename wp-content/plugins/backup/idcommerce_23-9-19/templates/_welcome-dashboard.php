<?php
$current_user = wp_get_current_user();
$user_firstname = $current_user->user_firstname;

$durl = md_get_durl();

// Get Wizard Details
$wizardDetails = getWizardDetails();

// Amount of Steps
$amountOfSteps = $wizardDetails['amoutofSteps'];

// Get Button Text
$btnText = $wizardDetails['currentStepText'];

// Set Classes
$profileClasses = $wizardDetails['currentClasses']['profileClasses'];
$projectClasses = $wizardDetails['currentClasses']['projectClasses'];
$paymentClasses = $wizardDetails['currentClasses']['paymentClasses'];

// Set URLS
$accountUrl = $wizardDetails['urls']['accountUrl'];
$paymentSettingsUrl = $wizardDetails['urls']['paymentSettingsUrl'];
$createProjectUrl = $wizardDetails['urls']['createProjectUrl'];
$currentStepUrl = $wizardDetails['urls']['currentStepUrl'];
$isShowWizard = $wizardDetails['isShowWizard'];

?>
<!-- IF all steps aren't complete, then show the wizard -->
<?php if($isShowWizard){ ?>

<div class="fb-welcome">
    <h1 class="fb-welcome--header">
        Welcome, <span><?php echo $user_firstname?></span>
    </h1>
    <div class="fb-welcome--description">
    Thanks for joining FundBlack. Let us help you get your campaign up and running. The following steps are your path to setting up your campaign.
    </div>
    <div class="fb-welcome--steps-wrapper">
    <div class="fb-welcome--steps">
        <a class="fb-welcome--step <?php echo ' '. $profileClasses; ?>" href="<?php echo $accountUrl; ?>">
            <div class="fb-welcome--step--img profile"></div>
            <break></break>
            <div class="fb-welcome--step--text">Complete <br>Profile</div>
        </a>
        <a class="fb-welcome--step <?php echo ' '. $paymentClasses; ?>" href="<?php echo $paymentSettingsUrl; ?>">
            <div class="fb-welcome--step--img credit-card"></div>
            <div class="fb-welcome--step--text">Enter Payment Info</div>
        </a>
        <a class="fb-welcome--step <?php echo ' '. $projectClasses; ?>" href="<?php echo $createProjectUrl; ?>">
            <div class="fb-welcome--step--img light-bulb"></div>
            <div class="fb-welcome--step--text">Create 1<sup>st</sup> <br/>Campaign</div>
        </a>
    </div>

    <div class="fb-welcome--steps__small">
        <a class="<?php echo 'fb-welcome--step__small ' . $profileClasses ?>" href="<?php echo $accountUrl; ?>">1</a>
        <a class="<?php echo 'fb-welcome--step__small ' . $paymentClasses ?>" href="<?php echo $paymentSettingsUrl; ?>">2</a>
        <a class="<?php echo 'fb-welcome--step__small ' . $projectClasses ?>" href="<?php echo $createProjectUrl; ?>">3</a>
    </div>
    <a class="fb-welcome--btn" href="<?php echo $currentStepUrl ?>" style="max-width: 300px;font-size: 24px !important;"><?php echo $btnText ?></a>
    </div>
</div>

<!-- If all steps are complete then show the project summary -->
<?php } else { ?>
<div class="md-profile fb-projects-summary">
        <?php
    $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
        echo '<div class="memberdeck fb--dashboard fb-projects">';
        echo '<ul class="md-box-wrapper full-width cf"><li class="md-box full"><div class="md-profile author-'.$user_id.'" data-author="'.$user_id.'">';
        echo '<ul>';
        $creator_args = array(
            'post_type' => 'ignition_product',
            'author' => $user_id,
            'posts_per_page' => -1,
            'post_status' => array('draft', 'pending', 'publish')
        );
        $user_projects = apply_filters('id_creator_projects', get_posts(apply_filters('id_creator_args', $creator_args)));
        if (!empty($user_projects)) {
            foreach ($user_projects as $post) {
                $post_id = $post->ID;
                $project_id = get_post_meta($post_id, 'ign_project_id', true);
                if (!empty($project_id)) {
                    $status = $post->post_status;
                    if (strtoupper($status) !== 'TRASH') {
                        $project = new ID_Project($project_id);
                        $the_project = $project->the_project();
                        $thumb = ID_Project::get_project_thumbnail($post_id, 'idc_dashboard_image_size');
                        if (empty($thumb)) {
                            $thumb = idcf_project_placeholder_image('thumb');
                        }
                        $project_raised = apply_filters('id_funds_raised', $project->get_project_raised(), $post_id);
                        $permalink = get_permalink($post_id);
                        if (strtoupper($status) !== 'PUBLISH') {
                            $permalink = $permalink.'&preview=true';
                        }
                        include IDC_PATH.'templates/_myProjects.php';
                    }
                }
            }
        }
        echo '</ul>';
        echo '</div>';
        ?>
    </div>
    <div class="creator-handbook-wrapper fb-creator-handbook">
        <h2>Creator Handbook</h2>
        <p class="handbook-description">Steps to Funding Coming Soon</p>
        <div class="handbook-item-wrapper">
            <div class="handbook-item"><span>1</span><div class="one"></div></div>
            <div class="handbook-item"><span>2</span><div class="two"></div></div>
            <div class="handbook-item"><span>3</span><div class="three"></div></div>
            <div class="handbook-item"><span>4</span><div class="four"></div></div>
        </div>
 </div>
</div>

    <?php }?>

<script>
	jQuery('.fb-welcome--step.disabled,.fb-welcome--step__small.disabled').on('click',function(e){
        e.preventDefault();
    })
</script>
