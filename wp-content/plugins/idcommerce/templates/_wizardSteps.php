<?php

// Get Wizard Details
$wizardDetails = getWizardDetails();

// Amount of Steps
$amountOfSteps = $wizardDetails['amoutofSteps'];
$currentStep = $wizardDetails['currentStep'];
$nextStep = $wizardDetails['nextStep'];
// Get Button Text
$btnText = $wizardDetails['currentStepText'];
$nextStepText = $wizardDetails['nextStepText'];
// Get Status
$currentStepStatus = $wizardDetails['currentStepStatus'];
// Set Classes
$profileClasses = $wizardDetails['currentClasses']['profileClasses'];
$projectClasses = $wizardDetails['currentClasses']['projectClasses'];
$paymentClasses = $wizardDetails['currentClasses']['paymentClasses'];
$nextBtnClasses = $wizardDetails['currentClasses']['nextBtnClasses'];

// Set URLS
$accountUrl = $wizardDetails['urls']['accountUrl'];
$paymentSettingsUrl = $wizardDetails['urls']['paymentSettingsUrl'];
$createProjectUrl = $wizardDetails['urls']['createProjectUrl'];
$nextStepUrl = $wizardDetails['urls']['nextStepUrl'];
$isShowWizard = $wizardDetails['isShowWizard'];
?>

<div class="fb-welcome" style="<?php echo $isShowWizard ? '' : 'display: none !important'?>">
		<div class="fb-welcome--steps-wrapper fb-welcome--steps-wrapper__prompt">
			<div class="fb-welcome--steps__small fb-welcome--steps__prompt <?php echo $nextBtnClasses == 'hidden' ? 'hidden-btn' : '' ?>">
				<a class="<?php echo 'fb-welcome--step__small ' . $profileClasses ?>" href="<?php echo $accountUrl; ?>">1<span>Profile</span></a>
				<a class="<?php echo 'fb-welcome--step__small ' . $paymentClasses ?>" href="<?php echo $paymentSettingsUrl; ?>">2<span class="payment">Payment</span></a>
        <a class="<?php echo 'fb-welcome--step__small ' . $projectClasses ?>" href="<?php echo $createProjectUrl; ?>">3<span>Project</span></a>
				<a class="fb-welcome--step__small fb-welcome--next-step-btn <?php echo $nextBtnClasses; ?>" href="<?php echo $nextStepUrl;?>">
					<div class="fb-welcome--step-of-steps"><?php echo "Next step is step $nextStep of $amountOfSteps"; ?></div>
					<div class="fb-welcome--step-text"><?php echo $nextStepText; ?></div>
				</a>
			</div>
			<div>
			</div>
		</div>
</div>

<script>
	jQuery('.fb-welcome--step.disabled,.fb-welcome--step__small.disabled').on('click',function(e){
        e.preventDefault();
    })
</script>
