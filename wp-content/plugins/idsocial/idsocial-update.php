<?php

/*
// TEMP: Enable update check on every request. Normally you don't need this! This is for testing only!
// NOTE: The 
//	if (empty($checked_data->checked))
//		return $checked_data; 
// lines will need to be commented in the check_for_plugin_update function as well.
*/
/*set_site_transient('update_plugins', null);

// TEMP: Show which variables are being requested when query plugin API
add_filter('plugins_api_result', 'aaa_result', 10, 3);
function aaa_result($res, $action, $args) {
	print_r($res);
	return $res;
}
// NOTE: All variables and functions will need to be prefixed properly to allow multiple plugins to be updated
*/

global $idsocial_api_url, $idsocial_plugin_slug;
$idsocial_api_url = 'https://ignitiondeck.com/id/pluginserv/';
$idsocial_plugin_slug = basename(dirname(__FILE__));

// Take over the update check
add_filter('pre_set_site_transient_update_plugins', 'idsocial_check_update', 20);

function idsocial_check_update($checked_data) {
	global $idsocial_api_url, $idsocial_plugin_slug, $wp_version;

	$plugin_file = $idsocial_plugin_slug .'/'. $idsocial_plugin_slug .'.php';

	//Comment out these two lines during testing.
	if (empty($checked_data->checked)) {
		return $checked_data;
	}

	$args = array(
		'slug' => $idsocial_plugin_slug,
		'version' => idsocial_current_version(),
	);

	// Start checking for an update
	$raw_response = idsocial_upate_info('basic_check', $args);

	if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200)) {
		$response = unserialize($raw_response['body']);
	}

	if (is_object($response) && !empty($response)) {
		$checked_data->response[$plugin_file] = $response;
	}
	
	return $checked_data;
}

// Take over the Plugin info screen
add_filter('plugins_api', 'idsocial_api_call', 10, 3);

function idsocial_api_call($def, $action, $args) {
	global $idsocial_plugin_slug, $idsocial_api_url, $wp_version;

	$plugin_file = $idsocial_plugin_slug .'/'. $idsocial_plugin_slug .'.php';

	if (!isset($args->slug) || ($args->slug !== $idsocial_plugin_slug)) {
		return $def;
	}

	$args->version = idsocial_current_version();

	$request = idsocial_upate_info($action, $args);

	if (is_wp_error($request)) {
		$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
	} else {
		$res = unserialize($request['body']);

		if ($res === false)
			$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
	}

	return $res;
}

function idsocial_upate_info($action, $args) {
	global $idsocial_api_url, $wp_version;
	$request_string = array(
		'body' => array(
			'action' => $action, 
			'request' => serialize($args)
		),
		'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
	);

	$request = wp_remote_post($idsocial_api_url, $request_string);
	return $request;
}

function idsocial_current_version() {
	global $idsocial_plugin_slug;
	$plugin_file = $idsocial_plugin_slug .'/'. $idsocial_plugin_slug .'.php';
	$plugin_info = get_plugin_data(WP_PLUGIN_DIR.'/'.$plugin_file);
	return $plugin_info['Version'];
}
?>