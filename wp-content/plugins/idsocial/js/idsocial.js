jQuery(document).ready(function() {
	jQuery('.social-share').click(function() {
		var item = jQuery(this).attr('id');
		switch(item) {
			case 'share-embed':
				jQuery('.social-button').toggle();
				jQuery('.embed-box').toggle();
				break;
			case 'share-link':
				var input = jQuery(this).data('input');
				jQuery('#' + input).parent().toggle();
				idSocialCopyShareLink(input);
				jQuery('#' + input).parent().toggle();
		}
	});
});

function idSocialCopyShareLink(input) {
	var el = document.getElementById(input);
	el.select();
	document.execCommand('Copy');
	alert(idsocial_strings.link + ': ' + el.value + ' ' + idsocial_strings.copied + '!');
}