<?php
/*
Template Name: Blog Page
*/
?>
<?php 
	$stellar_blog_title = get_the_title( get_option('page_for_posts', true) );
?>
<?php get_header(); ?>
<div id="container" class="blog">
	<h2 class="blog-title"><?php echo $stellar_blog_title; ?></h2>
	<div id="content" class="fullwidth">
		<?php get_template_part( 'loop', 'blog' ); ?>
		<?php get_template_part( 'nav', 'below' ); ?>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>