<?php
/*
Template Name: Home Page
*/
global $post;
$id = $post->ID;
if ($id > 0) {
     $content = the_project_content($id);
     $project_id = get_post_meta($id, 'ign_project_id', true);
     $settings = get_option('fivehundred_theme_settings');
     if (!empty($settings)) {
          $display_count = $settings['home_projects'];
     }
}
$num_projects = wp_count_posts('ignition_product');
$show_more = 0;
     if (!empty($num_projects->publish)) {
          $num_projects_pub = $num_projects->publish;
          if (isset($display_count) && $display_count < $num_projects_pub) {
               $show_more = 1;
          }
     }
?>
<?php if (isset($settings['home']) && !empty($settings['home'])) {
     get_header(); ?>
     <div id="container">
          <article id="content" class="ignition_project project-home">
               <?php get_template_part( 'project', 'content-home' ); ?>
          </article>
          <div class="clear"></div>
     </div>
     <?php get_footer(); ?>
<?php } else if (is_home()) { ?>
     <?php get_header(); ?>
          <div id="container">
               <div id="content">
               <!-- slider section -->
                    <?php get_template_part('loop','slider'); ?>
                    <!-- client logo section -->
                    <div class="clear"></div>
                    <?php get_template_part('loop','seen'); ?>
                    <div class="clear"></div>
                    <!-- project grid section -->
                    <?php do_action('fh_above_grid'); ?>
                    <div class="project-grid-wrapper">
                         <h2 class="entry-title"><?php _e('Latest Projects', 'fivehundredstellar'); ?></h2>
                         <div id="project-grid" class="home-grid">
                              <?php 
                              if (is_front_page()) {
                                   get_template_part('loop', 'project');
                              }
                              else {
                                   $paged = (get_query_var('paged') ? get_query_var('paged') : 1);
                                   $query = new WP_Query(array('paged' => 'paged', 'posts_per_page' =>1, 'paged' => $paged));
                                   // Start the loop
                                   if ( $query->have_posts() ) : 
                                        while ( $query->have_posts() ) : $query->the_post();
                                             get_template_part('entry');
                                        endwhile;
                                   endif; 
                                   wp_reset_postdata();
                              } ?>
                              <ul>
                                   <div class="ign-more-projects front-page">
                                        <a class="button" href="<?php echo get_post_type_archive_link('ignition_product'); ?>"><?php _e('All Projects', 'fivehundredstellar'); ?> <i class="fa fa-arrow-circle-right"></i></a>
                                   </div>
                              </ul>
                         </div>
                    </div>
                    <?php do_action('fh_below_grid'); ?>
             
                    <div class="clear"></div>
      
                         <!-- featured theme info -->
                         <?php get_template_part('loop','featured'); ?>
                         <div class="clear"></div>
     
                         <!-- Big CTA -->
                         <?php get_template_part('template','cta'); ?>
                         <div class="clear"></div>   

                    </div> <!-- content wrapper -->
               </div> <!-- container wrapper-->
               <!-- container end -->
          <div class="clear"></div>
<?php } else { ?>
     <?php get_header(); ?>
     <div id="container">
          <div id="content">
      
               <!-- slider section -->
      
               <?php get_template_part('loop','slider'); ?>
        
               <!-- Home Top Content Widget Area -->
               <div class="clear"></div>
               <?php if ( is_active_sidebar('home-top-content-widget-area') ) : ?>
                    <?php dynamic_sidebar('home-top-content-widget-area'); ?>
               <?php endif; ?>
               <div class="clear"></div>
               <!-- project grid section -->
               <?php do_action('fh_above_grid'); ?>
               <h2 class="entry-title"><?php _e('Latest Projects', 'fivehundredstellar'); ?></h2>
               <div id="project-grid" class="home-grid">
                    <?php 
                    if (is_front_page()) {
                         get_template_part('loop', 'project');
                    }
                    else {
                         $paged = (get_query_var('paged') ? get_query_var('paged') : 1);
                         $query = new WP_Query(array('paged' => 'paged', 'posts_per_page' =>1, 'paged' => $paged));

                         // Start the loop
                         if ( $query->have_posts() ) :
                              while ( $query->have_posts() ) : $query->the_post();
                                   get_template_part('entry');
                              endwhile;
                         endif; 
                         wp_reset_postdata();
                    } ?>
                    <div class="ign-more-projects front-page">
                         <a class="button" href="<?php echo get_post_type_archive_link('ignition_product'); ?>"><?php _e('All Projects', 'fivehundredstellar'); ?> <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
               </div>
               <?php do_action('fh_below_grid'); ?>
             
               <div class="clear"></div>
     
               <!-- Home Content Widget Area -->
               <?php if ( is_active_sidebar('home-content-widget-area') ) : ?>
                    <?php dynamic_sidebar('home-content-widget-area'); ?>
               <?php endif; ?>
               <div class="clear"></div>   

          </div> <!-- content wrapper -->
     </div> <!-- container wrapper-->
     <!-- container end -->
     <div class="clear"></div>
<?php } ?>
<?php get_footer(); ?>  