<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
    <div>
        <input type="text" id="s" name="s" value="" placeholder="<?php _e('Search', 'fivehundredstellar'); ?>" />
        <input type="submit" value="<?php _e('Search', 'fivehundredstellar'); ?>" id="searchsubmit" />
    </div>
</form>