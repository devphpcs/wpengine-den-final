<div class="entry-content">
	<?php 
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('singlepost-thumb', array('class' => 'singlepost-thumb'));
		} 
	?>
	<?php get_template_part( 'entry', 'meta' ); ?>
	<?php the_content(); ?>
</div>