<?php
global $post;
$id = $post->ID;
$summary = the_project_summary($id);
//$video = the_project_video($id);

$author = get_user_by( 'id', $post->post_author );
if (function_exists('is_id_pro') && is_id_pro() ) {
	$profile = ide_creator_info($post->ID);
}
$permalink_structure = get_option('permalink_structure');
$prefix = (empty($permalink_structure) ? '&' : '?');
do_action('fh_project_summary_before');
?>
<div class="ign-project-summary <?php echo (!empty($summary->successful) ? 'successful' : 'unsuccessful'); ?> <?php echo 'post-'.$id; ?>">
	<?php if ( $summary->successful) : ?>
          <div class="campaign-ribbon success">
               <a href="<?php the_permalink(); ?>"><?php _e( 'Successful', 'fivehundredstellar' ); ?></a>
          </div>
	<?php elseif ($summary->end_type == 'closed' && $summary->days_left <= '0') : ?>
          <div class="campaign-ribbon unsuccess">
               <a href="<?php the_permalink(); ?>"><?php _e( 'Unsuccessful', 'fivehundredstellar' ); ?></a>
          </div>
	<?php endif; ?>
	<a href="<?php echo the_permalink(); ?>">
          <div class="ign-summary-container">
               <div class="ign-summary-item">
                    <div class="ign-summary-image" style="background-image: url(<?php echo $summary->image_url; ?>)">	
                        <div class="ign-summary-learnmore"><span class="search"><i class="fa fa-search"></i></span></div> 
                    </div>
               </div>
               <div class="title">
                    <h3><?php echo $summary->name; ?></h3>
                    <div class="project-tag">
                    <?php
                    $terms = wp_get_post_terms( $post->ID, 'project_category');
				if(!empty($terms)) {
                         $site_url = home_url();
                         $cat_name = "";
                         foreach($terms as $term) {
                              if($term->count > 0) {
                                   $cat_name .= $term->name;
                                   break;
                              }
                         }
                         if ($term->count > 0) {
                              echo $cat_name;
                         }
                    } ?>
                    </div>
               </div>
               <h3 class="ign-summary-author">
                 	<span>
                         <?php _e('by', 'fivehundredstellar'); ?> 
     				<?php  if (function_exists('is_id_pro') && is_id_pro() ) { 
     					echo $profile['name'];  
     				}
     				else {
     				the_author();
     				} ?>
                    </span>
               </h3>
               <span class="ign-summary-desc"><?php echo $summary->short_description; ?></span>
               <?php if (!empty(apply_filters('fh_project_location', (isset($profile['location']) ? $profile['location'] : null)))) { ?>
                    <span class="ign-summary-loc contact-location"><i class="icon-link"></i> <?php echo make_clickable(apply_filters('fh_project_location', $profile['location'])); ?></span>
               <?php } ?>
               <div class="clear"></div>
               <div class="ign-progress-wrapper">
                    <div class="ign-progress-bar" style="width: <?php echo $summary->percentage.'%'; ?>"></div>
                    <div class="ign-progress-percentage"><?php echo $summary->percentage.'%'; ?> <span> <?php _e('Funded', 'fivehundredstellar'); ?></span></div>
  				<div class="ign-progress-raised">
                         <strong><?php echo $summary->total; ?></strong>
                         <div class="ign-raised">
                              <?php _e('Raised', 'fivehundredstellar'); ?>
                         </div>
                    </div>
               </div>
               <?php if ($summary->end_type == 'closed') { ?>
                    <div class="ign-summary-days">
                         <?php echo $summary->days_left; ?> <span> <?php _e('Days Left', 'fivehundredstellar'); ?></span>
                    </div>
               <?php } ?>
          </div>
     </a>
</div>