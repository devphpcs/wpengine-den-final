<?php
global $post;
$id = $post->ID;
//$hDeck = the_project_hDeck($id);
$project_id = get_post_meta($id, 'ign_project_id', true);
$end_type = get_post_meta($id, 'ign_end_type', true);
if (class_exists('Deck')) {
	$hdeck = new Deck($project_id);
	if (method_exists($hdeck, 'hDeck')) {
		$hDeck = $hdeck->hDeck();
	}
	else {
		$hDeck = the_project_hDeck($id);
	}
	$permalinks = get_option('permalink_structure');
	$summary = the_project_summary($id);
//	$video = the_project_video($id);
	do_action('fh_hDeck_before');
}
idcf_get_project();

?>

<?php if (isset($hDeck)) { ?>
<div id="ign-hDeck-wrapper">
	<div id="ign-hdeck-wrapperbg">
		<div id="ign-hDeck-header">
                  <div id="ign-hDeck-left">
                  		<div class="feature-image-wrapper">
                        	<div class="video feature-image <?php 
							$video = idcf_get_project_video();
							 echo (!empty($video) ? 'hasvideo' : ''); ?>" style="background-image: url(<?php idcf_the_project_image_url(); ?>)">
							 <?php echo $video; ?> </div>

                      </div>
                    </div>
                  <div id="ign-hDeck-right">
                      <div class="internal">
                          	<div class="ign-product-goal" style="clear: both;">
							 	 <strong><?php echo $summary->total; ?></strong>
							</div> 
                            <div class="ign-pledged">
								<?php _e('Pledged of', 'fivehundredstellar'); ?>
								<?php echo $summary->goal; ?>
								<?php _e('goal', 'fivehundredstellar'); ?>
                          </div>
                           <div class="ign-product-supporters">
                              <strong><?php echo $summary->pledgers; ?></strong>
                          </div>
                          <div class="ign-supporters"><?php _e('Backers', 'fivehundredstellar'); ?></div>
                          <?php if ($end_type == 'closed') { ?>
                          <div class="ign-days-left">
                              <strong><?php echo $summary->days_left; ?></strong>
                          </div>
                          <div><?php _e('Days Left', 'fivehundredstellar'); ?></div>
                          <?php } ?>
                          <div id="hDeck-right-bottom">
                            <div class="ign-supportnow" data-projectid="<?php echo $project_id; ?>">
                            
                                <?php if ($hDeck->end_type == 'closed' && $hDeck->days_left <= 0) {?>
                                
                                <a class="expired"><?php _e('Project Closed', 'fivehundredstellar'); ?></a>
                               
                                
                                <?php }
								else { ?>
                                
                                  <?php if (empty($permalinks) || $permalinks == '') { ?>
                                      <a href="<?php the_permalink(); ?>&purchaseform=500&amp;prodid=<?php echo (isset($project_id) ? $project_id : ''); ?>"><?php _e('Support Now', 'fivehundredstellar'); ?></a>
                                  <?php }
								
                                  else { ?>
                                  
                                      <a href="<?php the_permalink(); ?>?purchaseform=500&amp;prodid=<?php echo (isset($project_id) ? $project_id : ''); ?>"><?php _e('Support Now', 'fivehundredstellar'); ?></a>
                                  <?php } ?>
                                  <?php if ($end_type == 'closed') { ?>
                                    <div class="ign-project-end">
                                     <?php _e('Project Ends on', 'fivehundredstellar'); ?> <?php echo $hDeck->end; ?>
                                    </div>
                                  <?php } ?>
                            <?php }?>
                            </div>
                            
                        </div>
                        <div id="ign-share-button">
                        	<a href="#"><?php _e('Share Project', 'fivehundredstellar'); ?></a>
                        </div>
                        
                            <div id="ign-hDeck-social">
                            	<?php do_action('idf_general_social_buttons', $post->ID, ''); ?>
                           </div>
                        </div> <!-- internal -->
                  </div> <!-- hdeck-right -->
        </div>
	</div>
</div>

<?php } 
else { ?>
<div id="ign-hDeck-wrapper">
	<div id="ign-hdeck-wrapperbg">
		<div id="ign-hDeck-header">
			<div id="ign-hDeck-left">
			</div>
			<div id="ign-hDeck-right">
				<div class="internal">
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<?php } ?>