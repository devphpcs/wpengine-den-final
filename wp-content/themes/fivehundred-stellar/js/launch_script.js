jQuery(document).on('click', "#launch_campaign", function(e) {			    
		e.preventDefault(); 
		var post_id = jQuery(this).val();
		jQuery.ajax({
			 type : "post",
			 dataType : "json",
			 url : myAjax.ajaxurl,
			 data : {action: "launch_campaign", post_id : post_id, flag: 1},			
			 success: function(response) {				
				if(response.key == "1"){
					jQuery.toast({
                        heading: 'Campaigned Launched Successfully.',
                        position: 'top-right',
                        loaderBg:'#54c739',
                        icon: 'success',
                        hideAfter: 20000, 
                        stack: 6
                    });
				}
				/* else{
					jQuery.toast({
                        heading: 'Campaign is already launched',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 20000
                    });
				}	 */			
			 }
		});
});
	
jQuery(document).on('click', "#launch_btn", function(e) {	   		
		e.preventDefault(); 
		var post_id = jQuery(this).attr('data-post_id');		
		jQuery.ajax({
			type : "post",
			dataType : "json",
			url : myAjax.ajaxurl,
			data : {action: "launch_campaign", post_id : post_id, flag: 1},			 
			success: function(response) {	
				if(response.key == "1"){
					jQuery('.fb--dashboard .cf').before('<div class="ignitiondeck fb-notification-wrapper message"><p class="notification green"><strong>'+response.data+' Launched Successfully..!!</strong></p></div>');
					jQuery(this).css('display','none');					
				}
				/* else{
					jQuery('ul.project_listing').before('<div class="ignitiondeck fb-notification-wrapper message"><p class="notification green"><strong>Campaign Already Launched..!!</strong></p></div>');
				} */
				
			}
		});
});