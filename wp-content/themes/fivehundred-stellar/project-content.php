<?php
global $post;
$id = $post->ID;
$content = the_project_content($id);
$project_id = get_post_meta($id, 'ign_project_id', true);
if (function_exists('is_id_pro') && is_id_pro() ) {
	$profile = ide_creator_info($post->ID);
}
idcf_get_project();
?>
<?php
get_header();

?>
<div id="site-description" class="project-single">
  <?php echo idcf_get_project_title(); ?>
      <?php echo idcf_get_short_description(); ?>
      <span class="product-author-details">
        <span class="project-header-author"><i class="fa fa-user"></i>
  	  <?php  if (function_exists('is_id_pro') && is_id_pro() ) {
                    echo $profile['name'];
                }
                else {
                    the_author();
                }
            ?>
        </span>
            <?php
                  $terms = wp_get_post_terms( $post->ID, 'project_category');
                  if(!empty($terms)) {
                      ?>
                      <i class="fa fa-folder-open"></i>
                      <span class="project-tag-single">
                      <?php
                      $site_url = home_url();
                      $cat_name = "";
                      foreach($terms as $term){
                          if($term->count > 0){
                              $cat_name .= "<a href='".esc_url( $site_url )."/project-category/".$term->slug."'>".$term->name."</a>";
                              break;
                          }
                      }
                      if($term->count > 0){ echo $cat_name; }
                  }
            ?>
         </span>
        <span class="project-header-date"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></span>
      </span>
</div>
<div id="content" class="ignition_project">
	<?php get_template_part( 'project', 'hDeck' ); ?>
</div>
<div id="ign-project-content" class="ign-project-content">
  <div class="entry-content content_tab_container">
    <?php get_template_part('nav', 'above-project'); ?>
    <div class="ign-content-long content_tab description_tab active">
       <?php echo apply_filters('the_content', $content->long_description); ?>
    </div>
    <div id="updateslink" class="content_tab updates_tab">
      <?php echo apply_filters('fivehundred_updates', do_shortcode( '[project_updates product="'.$project_id.'"]')); ?>
    </div>
		<div id="faqlink" class="content_tab faq_tab">
      <?php echo apply_filters('fivehundred_faq', do_shortcode( '[project_faq product="'.$project_id.'"]')); ?>
    </div>
	<?php 
	
	if( is_user_logged_in() ){
		$current_user = wp_get_current_user();		
        $user_id = $current_user->ID;
		$creator_args = array(
            'post_type' => 'ignition_product',
            'author' => $user_id,
            'posts_per_page' => 1,
			'include'          => array( $post->ID ),
            'post_status' => array('draft', 'pending', 'publish')
        );
        $user_projects = apply_filters('id_creator_projects', get_posts(apply_filters('id_creator_args', $creator_args)));		
		
		$status_new = $user_projects[0]->post_status;
		$meta = get_post_meta($post->ID,'launch_campaign_flag_'.$post->ID, true);
		
		if( $status_new == 'publish' && $meta != 1){	
			echo '<div id="launch_button">
				<div class="ign-launch">             
					<button type="submit" name="launch_campaign" id="launch_campaign" value="'.$user_projects[0]->ID.'" style="background-color: #54c739;color: #fff;padding: 10px;border-radius: 10px;margin-bottom: 32px;align-content: center;">Launch Campaign</button>
				</div>                            
			</div>';
		} 
	}
	?>
    <div class="content_tab comments_tab">
      <?php comments_template('/comments.php', true); ?>
    </div>
    <?php do_action('fh_below_project', $project_id, $id); ?>
  </div>
  <?php get_template_part( 'project', 'sidebar' ); ?>
  <div class="clear"></div>
</div>
