<?php
/*
 * Template Name: Campainer Portal
 */
get_header(); 
global $wpdb;
?>
<?php if(is_user_logged_in()): ?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery("#master-select-all").click(function() {
				jQuery(".backer_list").attr('checked', 'checked');
				jQuery(".select_status").val('all');
			});
			jQuery("#master-clear-all").click(function() {
				jQuery(".backer_list").removeAttr('checked');
				jQuery(".select_status").val('');
			});
		});
	</script>
<div id="container">
	<div id="content">
		<?php
			$users = get_users( array( 'fields' => array( 'ID','display_name','user_email' ) ) );
			$current_user = wp_get_current_user();
			$current_user_email = $current_user->user_email;
		

			if( isset($_POST['submit_campaign_portal']) ){
				if( current_user_can('administrator') ){	
					$mail_to_arr = array();		
					$backers_list = $_POST['backer_name'];
					if( !empty($backers_list)){
						$select_status = $_POST['select_status'];
						$subject = $_POST['subject'];
						$message = $_POST['message'];
						$headers = 'From:'. $current_user_email;
						if(!empty($select_status)){
							foreach($users as $user){	
								if($user->user_email)
									$mail_to_arr[] = $user->user_email;
							}
							$to = implode(",",$mail_to_arr);
							$send = wp_mail( $to, $subject, $message, $headers );
							if($send){
								echo '<h2 style="color:green;">Message Send Successfully!!!</h2>';
							}else{
								echo '<h2 style="color:green;">Message Not Send Successfully!!!</h2>';
							}
						}else{				
							foreach($backers_list as $backer_list){
								$user_obj = get_user_by('id', $backer_list);
								if($user_obj)
									$mail_to_arr[] = $user_obj->user_email;	
							}
							$to = implode(",",$mail_to_arr);
							$send = wp_mail( $to, $subject, $message, $headers );
							if($send){
								echo '<h2 style="color:green;">Message Send Successfully!!!</h2>';
							}
							else{
								echo '<h2 style="color:green;">Message Not Send Successfully!!!</h2>';
							}
						}	
					}else{
						echo '<h2 style="color:red;">Please select user!!</h2>';
					}
				}else if( current_user_can('subscriber') ){	
					$mail_to_arr = array();
					$backers_list = $_POST['backer_name'];
					if( !empty($backers_list)){
							
						$select_status = $_POST['select_status'];
						$subject = $_POST['subject'];
						$message = $_POST['message'];
						$headers = 'From:'. $current_user_email;
						
						if(!empty($select_status)){			
							$user_id = get_current_user_id();
							$args = array(
								'post_type' => 'ignition_product',
								'post_status' => 'published',		
								'author' => $user_id
							);                       

							$wp_query = new WP_Query($args);
							while ( $wp_query->have_posts() ) : $wp_query->the_post(); 	
								global $wpdb;				
								$orders = $wpdb->get_results("select * from wp_ign_pay_info where product_id = (select id from wp_ign_products 	where product_name = '".get_the_title()."')");
								foreach ($orders as $order){
									if($order->user_email)
										$mail_to_arr[] = $order->user_email;	
								}
								$to = implode(",",$mail_to_arr);
								$send = wp_mail( $to, $subject, $message, $headers );
								if($send){
									echo '<h2 style="color:green;">Message Send Successfully!!!</h2>';
								}
								else{
									echo '<h2 style="color:red;">Message Not Send Successfully!!!</h2>';
								}
							endwhile; 
						}else{				
							foreach($backers_list as $backer_list){						
								$user_obj = get_user_by('id', $backer_list);
								if($user_obj)						
									$mail_to_arr[] = $user_obj->user_email;	
							}
							$to = implode(",",$mail_to_arr);
							$send = wp_mail( $to, $subject, $message, $headers );							
							if($send){
								echo '<h2 style="color:green;">Message Send Successfully!!!</h2>';
							}else{
								echo '<h2 style="color:red;">Message Not Send Successfullyy!!!</h2>';
							}
						}	
					}
					else{
						echo '<h2 style="color:red;">Please select user!!</h2>';
					}
				}		
			}
		?>

		<h2>Campaigner Portal Form</h2>
		<form method="POST" action="" id="campaigner-portal-settings" name="campaigner-portal-settings">		
			<input type="hidden" name="select_status" class="select_status" value=""/>
			<div>
				<h3>Subject</h3>
				<input type="text" name="subject" value="" size="50" required />
				<h3>Message Box</h3>		
				<textarea name="message" cols="50" rows="5" required></textarea><br />
				<input type="submit" name="submit_campaign_portal" value="Send Message" />
			</div>
			<div>
				<p>
					<a href="javascript:void;" id="master-select-all">Select All</a> &nbsp; 
					<a href="javascript:void;" id="master-clear-all" class="" style="color:#bc0b0b;">Clear All</a>
				</p>
			</div>
			<?php
				if( current_user_can('administrator') ){
					echo '<table width="100%">';
					$i = 1;
					foreach($users as $user_name){				
						if( $i == 1 ){
							echo '<tr>';						
						}else {
							echo '<td><input type="checkbox" class="backer_list" name="backer_name[]" value="'.$user_name->ID.'"/>';
							echo $user_name->display_name.'</td>';	
						}
						if( $i == 5){
							echo '</tr>';						
							$i = 1;
						}else{
							$i++;	
						}
										
					}
					echo '</table>';
				}else if( current_user_can('subscriber') ){
					$user_id = get_current_user_id();
					$args = array(
						'post_type' => 'ignition_product',
						'post_status' => 'published',		
						'author' => $user_id
					);              
					$table_name_pay_info = $wpdb->prefix."ign_pay_info";         
					$table_name_ign_products = $wpdb->prefix."ign_products";         

					$wp_query = new WP_Query($args);
					
					while ( $wp_query->have_posts() ) : $wp_query->the_post(); 			
						$orders = $wpdb->get_results("select * from $table_name_pay_info where product_id = (select id from $table_name_ign_products where product_name = '".get_the_title()."')");
						$i = 1;
						echo '<table width="100%">';
						foreach ($orders as $order){
							//echo $order->first_name;echo '</br>'.$order->last_name;
							if( $i == 1 ){
								echo '<tr>';							
							}else {
								echo '<td><input type="checkbox" class="backer_list" name="backer_name[]" value="'.$order->id.'"/>';
								echo $order->first_name.' '.$order->last_name.'</td>';	
							}
							if( $i == 5){
								echo '</tr>';						
								$i = 1;
							}else{
								$i++;	
							}
						}
						echo '</table>';
					endwhile;
				}
			?>
		</form>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>